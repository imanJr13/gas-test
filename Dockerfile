FROM backend-test-app:1.0.0

WORKDIR /app
COPY . /app

ENTRYPOINT ["python3"]

CMD ["view.py"]
